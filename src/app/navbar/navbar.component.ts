//import { Component, OnInit } from '@angular/core';

import { Component, LOCALE_ID, Inject } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  languages = [
  { code: 'en', label: 'English'},
  { code: 'fr', label: 'Français'}
];

  constructor(@Inject(LOCALE_ID) protected localeId: string) {}


}
