import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  life: number;
  goal: string;
  goalletter: string;
  key: string;
  step: number;
  maxsteps: number;
  running=false;
  message=0;

  constructor() { }

  ngOnInit(): void {
    this.life=3;
    this.maxsteps=10;
    this.step=0;
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (this.running){
    this.key = event.key.toUpperCase();
    this.verify();
    }
  }

  newGame(): void{
    this.running=true;
    this.goal=this.makegoal(this.maxsteps);
    this.life=3;
    this.step=0;
    this.message=1;
    this.goalletter=this.goal.charAt(this.step);
  }

  verify():void{
    if (this.key==this.goalletter && this.step<this.maxsteps){
      this.step++;
      if ( this.step==this.maxsteps){
        this.running=false;
        this.message=4;
      }
      else{
        this.message=2;
        this.goalletter=this.goal.charAt(this.step);
      }
    }
    else {
      this.life--
      if (this.life==0)
      {
        this.running=false;
        this.message=5;
      }
      else{
        this.message=3;
      }
    }
  }

  makegoal(length): string {
     var result           = '';
     var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
     //var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
     var charactersLength = characters.length;
     for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
     }
     return result;
  }

}



/*type(value,goal,step){

}*/
