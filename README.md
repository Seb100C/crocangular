# CrocLetter in Angular

https://crocangular.cabanes.dev/

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.6.

It's inspired by Happy Letters, an educational game developed by Amsoft in 1984 for Amstrad CPC.
This how I learnt how to use a keyboard and because I wanted my kid to have such activities, I thought it was a good idea to practice Angular while doing it.
